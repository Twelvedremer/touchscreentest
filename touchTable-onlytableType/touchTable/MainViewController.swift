//
//  MainViewController.swift
//  touchTable
//
//  Created by Momentum Lab 1 on 3/15/17.
//  Copyright © 2017 MomentumLab. All rights reserved.
//

import UIKit

let Kcell = "TableViewCell"
let Ktable1 = "TableA"
let Ktable2 = "TableB"

class MainViewController: UIViewController {
    
    var listImage:[UIImage] = [#imageLiteral(resourceName: "ChekBox_BS"),#imageLiteral(resourceName: "ChekBox_S"),#imageLiteral(resourceName: "ChekBox_Y"),#imageLiteral(resourceName: "ChekBox_G"),#imageLiteral(resourceName: "ChekBox_R"),#imageLiteral(resourceName: "ChekBox_B")]
    var initialIndexPath : NSIndexPath? = nil
    var snapshot : UIView? = nil
    @IBOutlet weak var table: UITableView!
    @IBOutlet var images: [UIImageView]!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.table.delegate = self
        self.table.register(UINib(nibName: Kcell, bundle: nil), forCellReuseIdentifier: Kcell)
        initEvents()
    }
    
    func initEvents(){
        for i in 0..<self.images.count {
            images[i].image = listImage[i]
            images[i].isUserInteractionEnabled = true
            let event = UILongPressGestureRecognizer(target: self, action: #selector(touchImageEvent(sender:)))
            images[i].addGestureRecognizer(event)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension MainViewController:UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Kcell) as! TableViewCell
        cell.load(image: #imageLiteral(resourceName: "ChekBox"), number: indexPath.row,delegate:self)
        return cell
    }
    
}

extension MainViewController:UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0
    }
}

extension MainViewController: cellEventDelegate{
    
    func touchCellEvent(sender gestureRecognizer: UIGestureRecognizer) {
        let longPress = gestureRecognizer as! UILongPressGestureRecognizer
        let state = longPress.state
        let locationInTable = longPress.location(in: self.table)
        let locationInView = longPress.location(in: self.view)
        let indexPath = self.table.indexPathForRow(at: locationInTable)
        switch state {
        case UIGestureRecognizerState.began:
            if let indexPath = indexPath ,
                let cell = self.table.cellForRow(at: indexPath) as? TableViewCell?,
                let snapImage = cell?.imageA {
                self.initialIndexPath = indexPath as NSIndexPath?
                self.snapshot  = snapshopOfImage(inputView: snapImage)
                var center = CGPoint(x: locationInView.x, y: locationInView.y)
                self.snapshot?.center = center
                self.snapshot?.alpha = 0.0
                self.view.addSubview(snapshot!)
                
                UIView.animate(withDuration: 0.25, animations: { () -> Void in
                    center.y = locationInView.y
                    center.x = locationInView.x
                    self.snapshot?.center = center
                    self.snapshot?.transform = CGAffineTransform(scaleX: 1.05, y: 1.05)
                    self.snapshot?.alpha = 0.98
                    
                }, completion: { (finished) -> Void in
                })
            }
        case UIGestureRecognizerState.changed:
            var center = self.snapshot!.center
            center.y = locationInView.y
            center.x = locationInView.x
            self.snapshot!.center = center
        default:
            if let indexPath = indexPath {
                let cell = self.table.cellForRow(at: self.initialIndexPath as! IndexPath) as! TableViewCell!
                let cellB = self.table.cellForRow(at: indexPath) as! TableViewCell!
                let auxImage = cell?.imageA.image
                cell?.imageA.image = cellB?.imageA.image
                cellB?.imageA.image = auxImage
                
            } else{
                for item in self.images {
                    if item.frame.contains(locationInView){
                        let cell = self.table.cellForRow(at: self.initialIndexPath as! IndexPath) as! TableViewCell!
                        let temporalImage = cell?.imageA.image
                        cell?.imageA.image = item.image
                        item.image = temporalImage
                        break;
                    }
                }
            }
            let center =  CGPoint(x: locationInView.x, y: locationInView.y)
            UIView.animate(withDuration: 0.25, animations: { () -> Void in
                self.snapshot!.center = (center)
                self.snapshot!.transform = .identity
                self.snapshot!.alpha = 0.0
                
            }, completion: { (finished) -> Void in
                if finished {
                    self.initialIndexPath = nil
                    self.snapshot!.removeFromSuperview()
                    self.snapshot = nil
                }
            })
        }
        
    }
    
    func touchImageEvent(sender gestureRecognizer: UIGestureRecognizer) {
        let longPress = gestureRecognizer as! UILongPressGestureRecognizer
        let state = longPress.state
        let locationInTable = longPress.location(in: table)
        let locationInView = longPress.location(in: self.view)
        let indexPath = table.indexPathForRow(at: locationInTable)
        let temporalImage = gestureRecognizer.view as! UIImageView
        switch state {
        case UIGestureRecognizerState.began:
            
            self.initialIndexPath = indexPath as NSIndexPath?
            self.snapshot  = snapshopOfImage(inputView: temporalImage)
            var center =  CGPoint(x: locationInView.x, y: locationInView.y)
            self.snapshot!.center = center
            self.snapshot!.alpha = 0.0
            self.view.addSubview(snapshot!)
            UIView.animate(withDuration: 0.25, animations: { () -> Void in
                center.y = locationInView.y
                self.snapshot!.center = center
                self.snapshot!.transform = CGAffineTransform(scaleX: 1.05, y: 1.05)
                self.snapshot!.alpha = 0.98
                
            }, completion: { (finished) -> Void in
            })
            
        case UIGestureRecognizerState.changed:
            var center = self.snapshot!.center
            center.y = locationInView.y
            center.x = locationInView.x
            self.snapshot!.center = center
        default:
            if let indexPath = indexPath ,
            let cell = self.table.cellForRow(at: indexPath) as? TableViewCell?,
            let bound = cell?.imageA?.bounds,
            self.view.convert(bound, from: cell?.contentView).contains(locationInView)
            {
                let auxImage = cell?.imageA.image
                cell?.imageA.image = temporalImage.image
                temporalImage.image = auxImage
            } else {
                for item in self.images {
                    if item.frame.contains(locationInView){
                        let auxImage = temporalImage.image
                        temporalImage.image = item.image
                        item.image = auxImage
                        break;
                    }
                }
                
            }
            
            let center =  CGPoint(x: locationInView.x, y: locationInView.y)
            UIView.animate(withDuration: 0.25, animations: { () -> Void in
                self.snapshot!.center = center
                self.snapshot!.transform = .identity
                self.snapshot!.alpha = 0.0
                
            }, completion: { (finished) -> Void in
                if finished {
                    self.initialIndexPath = nil
                    self.snapshot!.removeFromSuperview()
                    self.snapshot = nil
                }
            })
        }
    }
    
    func snapshopOfImage(inputView: UIView) -> UIView{
        UIGraphicsBeginImageContextWithOptions(inputView.bounds.size, false, 0.0)
        inputView.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()! as UIImage
        UIGraphicsEndImageContext()
        let cellSnapshot : UIView = UIImageView(image: image)
        cellSnapshot.layer.masksToBounds = false
        cellSnapshot.layer.cornerRadius = 0.0
        cellSnapshot.layer.shadowOffset = CGSize(width: -5.0, height: 0.0)
        cellSnapshot.layer.shadowRadius = 5.0
        cellSnapshot.layer.shadowOpacity = 0.4
        return cellSnapshot
    }
}

