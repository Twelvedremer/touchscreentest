//
//  TableViewCell.swift
//  touchTable
//
//  Created by Momentum Lab 1 on 3/15/17.
//  Copyright © 2017 MomentumLab. All rights reserved.
//

import UIKit

protocol cellEventDelegate {
    func touchCellEvent(sender:UIGestureRecognizer)
}

class TableViewCell: UITableViewCell {
    @IBOutlet weak var imageA: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    var delegate:cellEventDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.isUserInteractionEnabled = true
        self.imageA?.isUserInteractionEnabled = true
        initEvent()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    func load(image:UIImage,number:Int,delegate:cellEventDelegate){
        self.imageA.image = image
        self.titleLabel.text = "celda \(number+1)"
        self.delegate = delegate
    }
    
    func initEvent(){
        let event = UILongPressGestureRecognizer(target: self, action: #selector(touchImage(sender:)))
        event.cancelsTouchesInView = false
        self.imageA.addGestureRecognizer(event)
    }
    
    func touchImage(sender:UIGestureRecognizer){
        self.delegate.touchCellEvent(sender: sender)
    }
    
}
