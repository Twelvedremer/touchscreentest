//
//  MainViewController.swift
//  touchTable
//
//  Created by Momentum Lab 1 on 3/15/17.
//  Copyright © 2017 MomentumLab. All rights reserved.
//

import UIKit

let Kcell = "TableViewCell"
let Ktable1 = "TableA"
let Ktable2 = "TableB"

class MainViewController: UIViewController {
    
    var listImage:[UIImage] = [#imageLiteral(resourceName: "ChekBox_BS"),#imageLiteral(resourceName: "ChekBox_S"),#imageLiteral(resourceName: "ChekBox_Y"),#imageLiteral(resourceName: "ChekBox_G"),#imageLiteral(resourceName: "ChekBox_R"),#imageLiteral(resourceName: "ChekBox_B")]
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var slider: UISlider!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.table.delegate = self
        self.table.register(UINib(nibName: Kcell, bundle: nil), forCellReuseIdentifier: Kcell)
        initEvent()
    }
    
    func initEvent(){
        slider.minimumValue = 0
        slider.maximumValue = Float(listImage.count-1)
        let event = UITapGestureRecognizer(target: self, action: #selector(touchCellEvent(sender:)))
        table.addGestureRecognizer(event)
    }
   
    @IBAction func sliderChange(_ sender: UISlider) {
        let value = sender.value
        self.table.selectRow(at: IndexPath(row: Int(value), section: 0), animated: true, scrollPosition: .bottom)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func touchCellEvent(sender gestureRecognizer: UIGestureRecognizer){
        let location = gestureRecognizer.location(in: table)
        let indexPath = table.indexPathForRow(at: location)
        let cell = table.cellForRow(at: indexPath!) as! TableViewCell!
        slider.setValue(Float((cell?.index)!), animated: true)
        self.table.selectRow(at: IndexPath(row: (cell?.index)!, section: 0), animated: true, scrollPosition: .middle)
    }

}

extension MainViewController:UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listImage.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Kcell) as! TableViewCell
        cell.load(image: listImage[indexPath.row], number: indexPath.row)
        return cell
    }
    
}

extension MainViewController:UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0
    }
}


