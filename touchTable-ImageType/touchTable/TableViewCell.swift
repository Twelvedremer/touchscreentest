//
//  TableViewCell.swift
//  touchTable
//
//  Created by Momentum Lab 1 on 3/15/17.
//  Copyright © 2017 MomentumLab. All rights reserved.
//

import UIKit

protocol UIcellDelegate {
    func longPressGestureRecognizedImageA(sender gestureRecognizer: UIGestureRecognizer)
    func longPressGestureRecognizedImageB(sender gestureRecognizer: UIGestureRecognizer)
    func longPressGestureRecognizedCellA(sender gestureRecognizer: UIGestureRecognizer)
    func longPressGestureRecognizedCellB(sender gestureRecognizer: UIGestureRecognizer)
}

class TableViewCell: UITableViewCell {
    @IBOutlet weak var imageA: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    var type:Bool = false
    var delegate:UIcellDelegate? = nil
    var edited:Bool = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.isUserInteractionEnabled = true
        self.imageA?.isUserInteractionEnabled = true
        let eventImage = UILongPressGestureRecognizer(target: self, action: #selector(eventTouchImage(sender:)))
        eventImage.cancelsTouchesInView = false
        let eventCell = UILongPressGestureRecognizer(target: self, action: #selector(eventTouchCell(sender:)))
        eventCell.cancelsTouchesInView = false
        self.imageA.addGestureRecognizer(eventImage)
        self.addGestureRecognizer(eventCell)
           }
   
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    func load(image:UIImage,number:Int,type:Bool,delegate:UIcellDelegate){
        self.imageA.image = image
        self.titleLabel.text = "celda \(number+1)"
        self.type = type
        self.delegate = delegate
    }
    
    func eventTouchImage(sender gestureRecognizer: UIGestureRecognizer){
        if type{
            self.delegate?.longPressGestureRecognizedImageA(sender: gestureRecognizer)
        } else {
            self.delegate?.longPressGestureRecognizedImageB(sender: gestureRecognizer)
        }
    }
    
    func eventTouchCell(sender gestureRecognizer: UIGestureRecognizer){
        if type{
            self.delegate?.longPressGestureRecognizedCellA(sender: gestureRecognizer)
        } else {
            self.delegate?.longPressGestureRecognizedCellB(sender: gestureRecognizer)
        }
    }
}
