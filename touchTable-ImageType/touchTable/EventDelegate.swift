//
//  EventDelegate.swift
//  touchTable
//
//  Created by Momentum Lab 7 on 3/19/17.
//  Copyright © 2017 MomentumLab. All rights reserved.
//

import Foundation
import UIKit


extension MainViewController : UIcellDelegate{
    func longPressGestureRecognizedImageA(sender gestureRecognizer: UIGestureRecognizer) {
        let longPress = gestureRecognizer as! UILongPressGestureRecognizer
        let state = longPress.state
        let locationInTableA = longPress.location(in: tableA)
        let locationInTableB = longPress.location(in: tableB)
        let locationInView = longPress.location(in: self.view)
        let indexPathA = tableA.indexPathForRow(at: locationInTableA)
        let indexPathB = tableB.indexPathForRow(at: locationInTableB)
        
        switch state {
        case UIGestureRecognizerState.began:
            if let indexPathA = indexPathA,
                let cell = tableA.cellForRow(at: indexPathA) as? TableViewCell?,
                let image = cell?.imageA{
                
                self.initialIndexPath = indexPathA as NSIndexPath?
                self.snapshot  = snapshopOfImage(inputView: image)
                var center = CGPoint(x: locationInView.x, y: locationInView.y
                )
                
                self.snapshot!.center = center
                self.snapshot!.alpha = 0.0
                self.view.addSubview(self.snapshot!)
                
                UIView.animate(withDuration: 0.25, animations: { () -> Void in
                    center.y = locationInView.y
                    center.x = locationInView.x
                    self.snapshot!.center = center
                    self.snapshot!.transform = CGAffineTransform(scaleX: 1.05, y: 1.05)
                    self.snapshot!.alpha = 0.98
                    
                }, completion: { (finished) -> Void in
                })
            }
        case UIGestureRecognizerState.changed:
            if snapshot != nil {
                var center = self.snapshot!.center
                center.y = locationInView.y
                center.x = locationInView.x
                self.snapshot!.center = center
                if let indexPathA = indexPathA ,
                    indexPathA as NSIndexPath? != self.initialIndexPath,
                    let cell = tableA.cellForRow(at: indexPathA) as? TableViewCell?,
                    let bound = cell?.imageA.bounds,
                    self.view.convert(bound, from: cell?.contentView).contains(locationInView){
                    swap(&listImage[indexPathA.row], &listImage[initialIndexPath!.row])
                    tableA.moveRow(at: self.initialIndexPath! as IndexPath, to: indexPathA)
                    tableA.reloadData()
                    self.initialIndexPath = indexPathA as NSIndexPath?
                }
            }
        default:
            if snapshot != nil
            {
                if let indexPathB = indexPathB ,
                    let cell = tableB.cellForRow(at: indexPathB) as? TableViewCell?,
                    let bound = cell?.imageA.bounds,
                    self.view.convert(bound, from: cell?.contentView).contains(locationInView){
                    swap(&listImageB[(indexPathB.row)], &listImage[(self.initialIndexPath?.row)!])
                    tableB.reloadData()
                    tableA.reloadData()
                }
                let center =  CGPoint(x: locationInView.x, y: locationInView.y)
                UIView.animate(withDuration: 0.25, animations: { () -> Void in
                    self.snapshot!.center = (center)
                    self.snapshot!.transform = .identity
                    self.snapshot!.alpha = 0.0
                    
                }, completion: { (finished) -> Void in
                    if finished {
                        self.initialIndexPath = nil
                        self.snapshot!.removeFromSuperview()
                        self.snapshot = nil
                    }
                })
            }
        }
        
    }
    
    func longPressGestureRecognizedImageB(sender gestureRecognizer: UIGestureRecognizer) {
        let longPress = gestureRecognizer as! UILongPressGestureRecognizer
        let state = longPress.state
        let locationInTableB = longPress.location(in: tableB)
        let locationInTableA = longPress.location(in: tableA)
        let locationInView = longPress.location(in: self.view)
        let indexPathB = tableB.indexPathForRow(at: locationInTableB)
        let indexPathA = tableA.indexPathForRow(at: locationInTableA)
        switch state {
        case UIGestureRecognizerState.began:
            if let indexPathB = indexPathB {
                self.initialIndexPath = indexPathB as NSIndexPath?
                let cell = tableB.cellForRow(at: indexPathB) as! TableViewCell!
                let image = cell?.imageA
                self.snapshot  = snapshopOfImage(inputView: image!)
                var center =  CGPoint(x: locationInView.x, y: locationInView.y)
                self.snapshot!.center = center
                self.snapshot!.alpha = 0.0
                self.view.addSubview(snapshot!)
                
                UIView.animate(withDuration: 0.25, animations: { () -> Void in
                    center.y = locationInView.y
                    center.x = locationInView.x
                    self.snapshot!.center = center
                    self.snapshot!.transform = CGAffineTransform(scaleX: 1.05, y: 1.05)
                    self.snapshot!.alpha = 0.98
                    
                }, completion: { (finished) -> Void in
                })
            }
        case UIGestureRecognizerState.changed:
            if snapshot != nil {
                var center = self.snapshot!.center
                center.y = locationInView.y
                center.x = locationInView.x
                self.snapshot!.center = center
                
                if let indexPathB = indexPathB ,
                    indexPathB as NSIndexPath? != self.initialIndexPath,
                    let cell = tableB.cellForRow(at: indexPathB) as! TableViewCell!,
                    let bound = cell.imageA?.bounds,
                    self.view.convert(bound, from: cell.contentView).contains(locationInView) {
                    swap(&listImageB[indexPathB.row], &listImageB[initialIndexPath!.row])
                    tableB.moveRow(at: self.initialIndexPath! as IndexPath, to: indexPathB)
                    tableB.reloadData()
                    self.initialIndexPath = indexPathB as NSIndexPath?
                }
            }
        default:
            if snapshot != nil{
                if let indexPathA = indexPathA ,
                    let cell = tableA.cellForRow(at: indexPathA) as! TableViewCell!,
                    let bound = cell.imageA?.bounds,
                    self.view.convert(bound, from: cell.contentView).contains(locationInView){
                    swap(&listImageB[(self.initialIndexPath?.row)!], &listImage[(indexPathA.row)])
                    tableB.reloadData()
                    tableA.reloadData()
                }
                let center =  CGPoint(x: locationInView.x, y: locationInView.y)
                UIView.animate(withDuration: 0.25, animations: { () -> Void in
                    self.snapshot!.center = center
                    self.snapshot!.transform = .identity
                    self.snapshot!.alpha = 0.0
                    
                }, completion: { (finished) -> Void in
                    if finished {
                        self.initialIndexPath = nil
                        self.snapshot!.removeFromSuperview()
                        self.snapshot = nil
                    }
                })
            }
        }
    }
    
    func longPressGestureRecognizedCellA(sender gestureRecognizer: UIGestureRecognizer) {
        let longPress = gestureRecognizer as! UILongPressGestureRecognizer
        let state = longPress.state
        let locationInTableA = longPress.location(in: tableA)
        let locationInTableB = longPress.location(in: tableB)
        let locationInView = longPress.location(in: self.view)
        let indexPathA = tableA.indexPathForRow(at: locationInTableA)
        let indexPathB = tableB.indexPathForRow(at: locationInTableB)
        switch state {
        case UIGestureRecognizerState.began:
            if let indexPathA = indexPathA {
                self.initialIndexPath = indexPathA as NSIndexPath?
                guard let cell = tableA.cellForRow(at: indexPathA) else {
                    return ;
                }
                self.snapshot  = snapshopOfImage(inputView: cell)
                var center = cell.center
                self.snapshot!.center = center
                self.snapshot!.alpha = 0.0
                self.view.addSubview(snapshot!)
                
                UIView.animate(withDuration: 0.25, animations: { () -> Void in
                    center.y = locationInView.y
                    self.snapshot!.center = center
                    self.snapshot!.transform = CGAffineTransform(scaleX: 1.05, y: 1.05)
                    self.snapshot!.alpha = 0.98
                    
                }, completion: { (finished) -> Void in
                })
            }
        case UIGestureRecognizerState.changed:
            var center = self.snapshot!.center
            center.y = locationInView.y
            self.snapshot!.center = center
            if let indexPathA =  indexPathA,
                indexPathA as NSIndexPath? != self.initialIndexPath{
                swap(&listImage[indexPathA.row], &listImage[initialIndexPath!.row])
                tableA.moveRow(at: self.initialIndexPath! as IndexPath, to: indexPathA)
                tableA.reloadData()
                self.initialIndexPath = indexPathA as NSIndexPath?
            }
        default:
            if let indexPathB = indexPathB {
                let imagen = listImage[(self.initialIndexPath?.row)!]
                listImageB.insert(imagen, at: (indexPathB.row))
                listImage.remove(at: (self.initialIndexPath?.row)!)
                tableB.reloadData()
                tableA.reloadData()
            } else if indexPathB == nil , indexPathA == nil, listImageB.count == 0, tableB.bounds.contains(locationInTableB){
                let imagen = listImage[(self.initialIndexPath?.row)!]
                listImageB.append(imagen)
                listImage.remove(at: (self.initialIndexPath?.row)!)
                tableB.reloadData()
                tableA.reloadData()
            }
            let center =  CGPoint(x: locationInView.x, y: locationInView.y)
            UIView.animate(withDuration: 0.25, animations: { () -> Void in
                self.snapshot!.center = (center)
                self.snapshot!.transform = .identity
                self.snapshot!.alpha = 0.0
                
            }, completion: { (finished) -> Void in
                if finished {
                    self.initialIndexPath = nil
                    self.snapshot!.removeFromSuperview()
                    self.snapshot = nil
                }
            })
        }
        
    }
    
    func longPressGestureRecognizedCellB(sender gestureRecognizer: UIGestureRecognizer) {
        let longPress = gestureRecognizer as! UILongPressGestureRecognizer
        let state = longPress.state
        let locationInTableB = longPress.location(in: tableB)
        let locationInTableA = longPress.location(in: tableA)
        let locationInView = longPress.location(in: self.view)
        let indexPathB = tableB.indexPathForRow(at: locationInTableB)
        let indexPathA = tableA.indexPathForRow(at: locationInTableA)
        switch state {
        case UIGestureRecognizerState.began:
            if let indexPathB = indexPathB {
                self.initialIndexPath = indexPathB as NSIndexPath?
                guard let cell = tableB.cellForRow(at: indexPathB) else {
                    return;
                }
                self.snapshot  = snapshopOfImage(inputView: cell)
                var center =  CGPoint(x: locationInView.x, y: locationInView.y)
                self.snapshot!.center = center
                self.snapshot!.alpha = 0.0
                self.view.addSubview(snapshot!)
                
                UIView.animate(withDuration: 0.25, animations: { () -> Void in
                    center.y = locationInView.y
                    self.snapshot!.center = center
                    self.snapshot!.transform = CGAffineTransform(scaleX: 1.05, y: 1.05)
                    self.snapshot!.alpha = 0.98
                    
                }, completion: { (finished) -> Void in
                })
            }
        case UIGestureRecognizerState.changed:
            var center = self.snapshot!.center
            center.y = locationInView.y
            self.snapshot!.center = center
            if let indexPathB = indexPathB,
                (indexPathB as NSIndexPath? != self.initialIndexPath) {
                swap(&listImageB[indexPathB.row], &listImageB[initialIndexPath!.row])
                tableB.moveRow(at: self.initialIndexPath! as IndexPath, to: indexPathB)
                tableB.reloadData()
                self.initialIndexPath = indexPathB as NSIndexPath?
            }
        default:
            if let indexPathA = indexPathA {
                let imagen = listImageB[(self.initialIndexPath?.row)!]
                listImage.insert(imagen, at: (indexPathA.row))
                listImageB.remove(at: (self.initialIndexPath?.row)!)
                tableB.reloadData()
                tableA.reloadData()
            } else if indexPathA == nil , indexPathB == nil, listImage.count == 0, tableA.bounds.contains(locationInTableA){
                let imagen = listImageB[(self.initialIndexPath?.row)!]
                listImage.append(imagen)
                listImageB.remove(at: (self.initialIndexPath?.row)!)
                tableB.reloadData()
                tableA.reloadData()
            }
            
            let center =  CGPoint(x: locationInView.x, y: locationInView.y)
            UIView.animate(withDuration: 0.25, animations: { () -> Void in
                self.snapshot!.center = center
                self.snapshot!.transform = .identity
                self.snapshot!.alpha = 0.0
                
            }, completion: { (finished) -> Void in
                if finished {
                    self.initialIndexPath = nil
                    self.snapshot!.removeFromSuperview()
                    self.snapshot = nil
                }
            })
        }
    }
    
    
    
    func snapshopOfImage(inputView: UIView) -> UIView {
        UIGraphicsBeginImageContextWithOptions(inputView.bounds.size, false, 0.0)
        inputView.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()! as UIImage
        UIGraphicsEndImageContext()
        let cellSnapshot : UIView = UIImageView(image: image)
        cellSnapshot.layer.masksToBounds = false
        cellSnapshot.layer.cornerRadius = 0.0
        cellSnapshot.layer.shadowOffset = CGSize(width: -5.0, height: 0.0)
        cellSnapshot.layer.shadowRadius = 5.0
        cellSnapshot.layer.shadowOpacity = 0.4
        return cellSnapshot
    }
}

