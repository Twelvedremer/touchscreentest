//
//  MainViewController.swift
//  touchTable
//
//  Created by Momentum Lab 1 on 3/15/17.
//  Copyright © 2017 MomentumLab. All rights reserved.
//

import UIKit

let Kcell = "TableViewCell"
let Ktable1 = "TableA"
let Ktable2 = "TableB"

class MainViewController: UIViewController {
    
    var listImage:[UIImage] = [#imageLiteral(resourceName: "ChekBox_BS"),#imageLiteral(resourceName: "ChekBox_S"),#imageLiteral(resourceName: "ChekBox_Y")]
    var listImageB:[UIImage] = [#imageLiteral(resourceName: "ChekBox_G"),#imageLiteral(resourceName: "ChekBox_R"),#imageLiteral(resourceName: "ChekBox_B")]
    var initialIndexPath : NSIndexPath? = nil
    var snapshot : UIView? = nil
    
    @IBOutlet weak var tableA: UITableView!
    @IBOutlet weak var tableB: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableA.delegate = self
        self.tableB.delegate = self
        self.tableA.register(UINib(nibName: Kcell, bundle: nil), forCellReuseIdentifier: Kcell)
        self.tableB.register(UINib(nibName: Kcell, bundle: nil), forCellReuseIdentifier: Kcell)
        
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension MainViewController:UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.restorationIdentifier == Ktable1{
            return listImage.count
        } else if tableView.restorationIdentifier == Ktable2{
            return listImageB.count
        } else {
            return 0
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Kcell) as! TableViewCell
        if tableView.restorationIdentifier == Ktable1{
            cell.load(image: listImage[indexPath.row],number:indexPath.row,type: true,delegate: self)
        } else if tableView.restorationIdentifier == Ktable2{
            cell.load(image: listImageB[indexPath.row],number:indexPath.row,type: false,delegate: self)
        }
        return cell
    }
    
    
}

extension MainViewController:UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0
    }
}
