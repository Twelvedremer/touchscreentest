//
//  MainViewController.swift
//  touchTable
//
//  Created by Momentum Lab 1 on 3/15/17.
//  Copyright © 2017 MomentumLab. All rights reserved.
//

import UIKit

let Kcell = "TableViewCell"
let Ktable1 = "TableA"
let Ktable2 = "TableB"

class MainViewController: UIViewController {
    
    var listImage:[UIImage] = [#imageLiteral(resourceName: "ChekBox_BS"),#imageLiteral(resourceName: "ChekBox_S"),#imageLiteral(resourceName: "ChekBox_Y")]
    var listImageB:[UIImage] = [#imageLiteral(resourceName: "ChekBox_G"),#imageLiteral(resourceName: "ChekBox_R"),#imageLiteral(resourceName: "ChekBox_B")]
    var initialIndexPath : NSIndexPath? = nil
    var cellSnapshot : UIView? = nil


    @IBOutlet weak var tableA: UITableView!
    @IBOutlet weak var tableB: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableA.delegate = self
        self.tableB.delegate = self
        self.tableA.register(UINib(nibName: Kcell, bundle: nil), forCellReuseIdentifier: Kcell)
        self.tableB.register(UINib(nibName: Kcell, bundle: nil), forCellReuseIdentifier: Kcell)
        initEvents()
    }
    
    func initEvents(){
        let eventA = UILongPressGestureRecognizer(target: self, action: #selector(longPressGestureRecognizedTableA(sender:)))
        let eventB = UILongPressGestureRecognizer(target: self, action: #selector(longPressGestureRecognizedTableB(sender:)))
        self.tableA.addGestureRecognizer(eventA)
        self.tableB.addGestureRecognizer(eventB)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension MainViewController:UITableViewDataSource{

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.restorationIdentifier == Ktable1{
            return listImage.count
        } else if tableView.restorationIdentifier == Ktable2{
            return listImageB.count
        } else {
            return 0
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Kcell) as! TableViewCell
        if tableView.restorationIdentifier == Ktable1{
            cell.load(image: listImage[indexPath.row],number:indexPath.row)
        } else if tableView.restorationIdentifier == Ktable2{
            cell.load(image: listImageB[indexPath.row],number:indexPath.row)
        }
        
        //cell.backgroundColor = listColor[indexPath.row]
        return cell
    }
    
}

extension MainViewController:UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0
    }
}

extension MainViewController{
    func longPressGestureRecognizedTableA(sender gestureRecognizer: UIGestureRecognizer) {
        let longPress = gestureRecognizer as! UILongPressGestureRecognizer
        let state = longPress.state
        let locationInTableA = longPress.location(in: tableA)
        let locationInTableB = longPress.location(in: tableB)
        let locationInView = longPress.location(in: self.view)
        let indexPathA = tableA.indexPathForRow(at: locationInTableA)
        let indexPathB = tableB.indexPathForRow(at: locationInTableB)
        switch state {
        case UIGestureRecognizerState.began:
            if indexPathA != nil {
                self.initialIndexPath = indexPathA as NSIndexPath?
                let cell = tableA.cellForRow(at: indexPathA!) as! TableViewCell!
                self.cellSnapshot  = snapshopOfCell(inputView: cell!)
                var center = cell?.center
                self.cellSnapshot!.center = center!
                self.cellSnapshot!.alpha = 0.0
                self.view.addSubview(cellSnapshot!)

                UIView.animate(withDuration: 0.25, animations: { () -> Void in
                    center?.y = locationInView.y
                    self.cellSnapshot!.center = center!
                    self.cellSnapshot!.transform = CGAffineTransform(scaleX: 1.05, y: 1.05)
                    self.cellSnapshot!.alpha = 0.98
                    
                    }, completion: { (finished) -> Void in
                })
            }
        case UIGestureRecognizerState.changed:
            var center = self.cellSnapshot!.center
            center.y = locationInView.y
            self.cellSnapshot!.center = center
            if ((indexPathA != nil) && (indexPathA as NSIndexPath? != self.initialIndexPath)) {
               swap(&listImage[indexPathA!.row], &listImage[initialIndexPath!.row])
               tableA.moveRow(at: self.initialIndexPath! as IndexPath, to: indexPathA!)
                tableA.reloadData()
               self.initialIndexPath = indexPathA as NSIndexPath?
            }
        default:
            if indexPathB != nil {
                let imagen = listImage[(self.initialIndexPath?.row)!]
                listImageB.insert(imagen, at: (indexPathB?.row)!)
                listImage.remove(at: (self.initialIndexPath?.row)!)
                tableB.reloadData()
                tableA.reloadData()
            } else if indexPathB == nil , indexPathA == nil, listImageB.count == 0, tableB.bounds.contains(locationInTableB){
                let imagen = listImage[(self.initialIndexPath?.row)!]
                listImageB.append(imagen)
                listImage.remove(at: (self.initialIndexPath?.row)!)
                tableB.reloadData()
                tableA.reloadData()
            }
            let center =  CGPoint(x: locationInView.x, y: locationInView.y)
            UIView.animate(withDuration: 0.25, animations: { () -> Void in
                self.cellSnapshot!.center = (center)
                self.cellSnapshot!.transform = .identity
                self.cellSnapshot!.alpha = 0.0
                
                }, completion: { (finished) -> Void in
                    if finished {
                        self.initialIndexPath = nil
                        self.cellSnapshot!.removeFromSuperview()
                        self.cellSnapshot = nil
                    }
            })
        }
        
    }
    
    func longPressGestureRecognizedTableB(sender gestureRecognizer: UIGestureRecognizer) {
        let longPress = gestureRecognizer as! UILongPressGestureRecognizer
        let state = longPress.state
        let locationInTableB = longPress.location(in: tableB)
        let locationInTableA = longPress.location(in: tableA)
        let locationInView = longPress.location(in: self.view)
        let indexPathB = tableB.indexPathForRow(at: locationInTableB)
        let indexPathA = tableA.indexPathForRow(at: locationInTableA)
        switch state {
        case UIGestureRecognizerState.began:
            if indexPathB != nil {
                self.initialIndexPath = indexPathB as NSIndexPath?
                let cell = tableB.cellForRow(at: indexPathB!) as! TableViewCell!
                self.cellSnapshot  = snapshopOfCell(inputView: cell!)
                var center =  CGPoint(x: locationInView.x, y: locationInView.y)
                self.cellSnapshot!.center = center
                self.cellSnapshot!.alpha = 0.0
                self.view.addSubview(cellSnapshot!)
                
                UIView.animate(withDuration: 0.25, animations: { () -> Void in
                    center.y = locationInView.y
                    self.cellSnapshot!.center = center
                    self.cellSnapshot!.transform = CGAffineTransform(scaleX: 1.05, y: 1.05)
                    self.cellSnapshot!.alpha = 0.98
                    
                    }, completion: { (finished) -> Void in
                })
            }
        case UIGestureRecognizerState.changed:
            var center = self.cellSnapshot!.center
            center.y = locationInView.y
            self.cellSnapshot!.center = center
            if ((indexPathB != nil) && (indexPathB as NSIndexPath? != self.initialIndexPath)) {
                swap(&listImageB[indexPathB!.row], &listImageB[initialIndexPath!.row])
                tableB.moveRow(at: self.initialIndexPath! as IndexPath, to: indexPathB!)
                tableB.reloadData()
                self.initialIndexPath = indexPathB as NSIndexPath?
            }
        default:
            if indexPathA != nil {
                let imagen = listImageB[(self.initialIndexPath?.row)!]
                listImage.insert(imagen, at: (indexPathA?.row)!)
                listImageB.remove(at: (self.initialIndexPath?.row)!)
                tableB.reloadData()
                tableA.reloadData()
            } else if indexPathA == nil , indexPathB == nil, listImage.count == 0, tableA.bounds.contains(locationInTableA){
                let imagen = listImageB[(self.initialIndexPath?.row)!]
                listImage.append(imagen)
                listImageB.remove(at: (self.initialIndexPath?.row)!)
                tableB.reloadData()
                tableA.reloadData()
            }
            
            let center =  CGPoint(x: locationInView.x, y: locationInView.y)
            UIView.animate(withDuration: 0.25, animations: { () -> Void in
                self.cellSnapshot!.center = center
                self.cellSnapshot!.transform = .identity
                self.cellSnapshot!.alpha = 0.0
         
                }, completion: { (finished) -> Void in
                    if finished {
                        self.initialIndexPath = nil
                        self.cellSnapshot!.removeFromSuperview()
                        self.cellSnapshot = nil
                    }
            })
        }
    }

    
    func snapshopOfCell(inputView: UIView) -> UIView {
        UIGraphicsBeginImageContextWithOptions(inputView.bounds.size, false, 0.0)
        inputView.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()! as UIImage
        UIGraphicsEndImageContext()
        let cellSnapshot : UIView = UIImageView(image: image)
        cellSnapshot.layer.masksToBounds = false
        cellSnapshot.layer.cornerRadius = 0.0
        cellSnapshot.layer.shadowOffset = CGSize(width: -5.0, height: 0.0)
        cellSnapshot.layer.shadowRadius = 5.0
        cellSnapshot.layer.shadowOpacity = 0.4
        return cellSnapshot
    }
}

