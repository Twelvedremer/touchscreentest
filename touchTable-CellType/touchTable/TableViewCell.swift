//
//  TableViewCell.swift
//  touchTable
//
//  Created by Momentum Lab 1 on 3/15/17.
//  Copyright © 2017 MomentumLab. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {
    @IBOutlet weak var imageA: UIImageView!
   
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
   
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    func load(image:UIImage,number:Int){
        self.imageA.image = image
        self.titleLabel.text = "celda \(number+1)"
    }
    
}
